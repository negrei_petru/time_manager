class Task < ActiveRecord::Base
  attr_accessible :desc, :from, :user_id, :status, :title, :type, :until
  belongs_to :user
  self.inheritance_column = :_type_disabled
end
