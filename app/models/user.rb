class User < ActiveRecord::Base
  attr_accessible :avatar, :username
  has_many :tasks
end
