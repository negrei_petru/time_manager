class TasksController < ApplicationController
	respond_to :json

	def index
		@tasks = Task.all
		respond_with @tasks
	end

	def show
		@task = Task.find(param[:id])
		respond_with @task
	end

	def create
		@task = Task.create(params[:task])
		respond_with @task
	end

	def update
		# @task = Task.find( params[:id] )
		# params[:task][:user_id] = nil if params[:task][:user_id].nil?
		# @task.update_attributes( params[:task] )
		@task = Task.update(params[:id], params[:task])
		render :json => @task # respond 200 instead of 204
	end

	def destroy
		# respond_with Task.destroy(params[:id])
		@task = Task.find( params[:id] )
		@task.destroy	
		respond_with @task
	end

end
