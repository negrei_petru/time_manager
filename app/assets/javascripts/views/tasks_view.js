App.DragableTask = Ember.View.extend({
	tagName:"li",
	classNames: ["actives"],

	didInsertElement: function(){
	    var taskId = this.get('content').get('id');
	    // console.log(App.User.find().content)

	    this.$().draggable({
	      start: function( event, ui ) {
	        $(this).css( 'z-index', 1000 );
	        $(this).data("taskId", taskId);
	      },
	      stop: function( event, ui ) {
	        $(this).css( 'z-index', 1 );
	      },
	      revert: function( droppableObject ) {
	        if( droppableObject !== false &&
	          droppableObject.hasClass("ui-droppable") ) {
	          return false;
	        }
	        else
	        {
	          return true;  
	        }
	      }
	    });
	}
});

App.DropableStatus = Ember.View.extend({
	tagName:"ul",
	classNames: ["nav"],
 
  didInsertElement: function(e)  {
    var status    =  this.$().closest('div').attr('id')
    var controller = this.get('controller');

    this.$().droppable({
      accept: ".ui-draggable",
      hoverClass: "droppablehover",
      drop: function( e, ui ) {
        controller.setupStatus( $(ui.draggable).data("taskId"), status);
      }
    });
  }
});

// setting user for a task
App.UserList = Ember.View.extend({
	tagName:"li",

	didInsertElement: function(){
		var user = this.get('content');
		var controller = this.get('controller');
		var taskId    =  this.$().closest('form').attr('id')

		this.$().find('a').on('click',function(e){
		 	e.preventDefault()
		 	controller.setupUser(taskId, user)
		 });
	}
});

// edit, new setting type of task
App.TypeViews = Ember.View.extend({
	tagName:"a",

	didInsertElement: function(){
		var task = this.get('content');
		var controller = this.get('controller');
		
		this.$().on('click',function(e){
		 	e.preventDefault();
			var type    =  $(this).closest('li').attr('class')
		 	controller.setupType(task, type)
		 });
	}
});

App.TimeViews = Ember.View.extend({
	tagName:"a",

	didInsertElement: function(){
		var task = this.get('content');
		var controller = this.get('controller');
		
		var tasks = task.get('user.sorted_tasks');
		console.log(tasks)
		this.$().on('click',function(e){
		 	e.preventDefault()
			var time =  $(this).closest('li').attr('class');
			if (tasks !== null && time ==='after'){
				// to find the last task for this user
				var array = tasks.getEach('end_time');
				var lasttask = Math.max.apply( Math, array );
				// ading 5 minutes
				var now = new Date();
				var time = ((lasttask - now.getTime())/60/1000)+5;
			}

			// console.log(time)
		 	controller.setupTime(task, time);
		 });
	}
});
