App.GraphShow = Ember.View.extend({
	classNames: ["containers"],

	didInsertElement: function(){
		var userId = this.get('content').get('datas');

	    $.plot(this.$(), userId, {
	        series: {
	            pie: {
	                show: true
	            }
	        },
	        grid: {
	            hoverable: true
	        },
	        legend: {
	            labelBoxBorderColor: "none"
	        }
	    });
	    // delete the legend and hover state
	    this.$().find(".legend").remove()
	    this.$().bind("plothover", pieHover);
	}
});

App.Rating = Ember.View.extend({
	tagName:"li",
	classNames: ["rating"],
	
	didInsertElement: function(){

		// this finds what user has the most tasks
		// UsersIndexController
		var controller = this.get('controller');
		var tasksTotal = controller.totalTaskss()
		var biggest = Math.max.apply(Math, tasksTotal);

		// here we get the current and completed tasks.
		var totalTasks = this.get('content').get('total');
		var currentTasks = this.get('content').get('current');
		var completedTasks = this.get('content').get('completed');


		var width = $('.ratings').width()

		// calculate the global step 
		if (totalTasks > 0 ){
			var tstep = ( width - 10)/ biggest;
			var cstep = ( width )/ biggest-18;
		} else {
			// need to improve later
			tstep = 0
		}

		// creating the objects to append
		this.$().animate({width: totalTasks * tstep});

		if (currentTasks > 0){
		 $('<div></div>',{
		    text: currentTasks,
		    class: 'current',
		    style : 'background-color: #4572A7'
		  }).appendTo(this.$()).animate({width: currentTasks * cstep})
		}

		if (completedTasks>0){
		 $('<div></div>',{
		    text: completedTasks,
		    class: 'current',
		    style : 'background-color: #89A54E'
		  }).appendTo(this.$()).animate({width: completedTasks * cstep})
		}
	}
});
