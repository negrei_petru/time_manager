App.Indexuser = Ember.View.extend({
	tagName:"li"
});

App.Taskuser = Ember.View.extend({
	tagName:"div",
	classNameBindings: ['task.title'],

	didInsertElement: function(){
		var current = false

		var task   =  this.get('content'); 
		var controller = this.get('controller');
		var selector = this.$();

	    var from = task.get('from')
	    var until = task.get('until')

		var startTime = new Date(from);
		var endTime = startTime.getTime() + ( until * 60 * 1000);

		var now = new Date()
		if (startTime.getTime() < now.getTime()  &&  now< endTime){
			current = true
		}
		
		selector.animate({width:(until)* 2});
		
		setTimeout(function(){
			if (current){
						timer(task,endTime, selector);
			} else {
				var reamains = startTime.getTime() - now.getTime()
					untiltimer(startTime.getTime(),selector)

				var timerId = setTimeout(function(){
					timer(task,endTime, selector);
				}, reamains);
			}
		},100);
	}
});

App.Timeline = Ember.View.extend({
	tagName:"li",

	didInsertElement: function(){
		var selector = this.$();
	    var timer = this.$().find(".timer");
		
		var morning = new Date()
		morning.setHours(8);
		morning.setMinutes(0);

		setTimeout(function(){
			timerline(morning,timer)
		},200);
	}
});
