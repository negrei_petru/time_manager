
function pieHover(event, pos, obj) {
    if (!obj)
        return;
 
    percent = parseFloat(obj.series.percent).toFixed(2);
    $("#pieHover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
}

window.timerlist = new Array();

function timer(task, endTime, selector){
	var timerId = setInterval(function(){
		var now = new Date()
		var remainingTime = endTime - now.getTime();
			// console.log('timer')
		if (remainingTime <= 0) {
			task.set('status','completed')
			task.store.commit();
			selector.remove()
			clearInterval(timerId);
			console.log('finished');
		} else {
			selector.animate({width:(remainingTime/60/1000) * 2});
		}
	}, 1000);
	timerlist.push(timerId)
}

function timerline(starttime, selector){
	var timerId = setInterval(function(){
		var now = new Date()
		var remainingTime = now.getTime() - starttime;
		if (remainingTime <= 0) {
			clearInterval(timerId);
			console.log('finished');
		} else {
			selector.animate({'margin-left':-(remainingTime/60/1000) * 2});
		}
	}, 1000);
	timerlist.push(timerId)
}

function untiltimer(endTime, selector){
	var timerId = setInterval(function(){
		var now = new Date()
		var remainingTime = endTime - now.getTime();
		console.log('timer')
		if (remainingTime <= 0) {
			clearInterval(timerId);
			console.log('finishedlog');
		} else {
			selector.animate({'left': (remainingTime/60/1000) *2 });
		}
	}, 1000);
	timerlist.push(timerId)
}

function stopTimer() {
	$.each(window.timerlist, function(index, value) {
 		clearInterval(value)
	});
}

$(window).bind('hashchange', function() {
	// if ( window.location.hash == '#/'){
	// 	window.location.reload()
	// }
	// console.log(window.location.href)
	stopTimer();
	// window.location.replace(window.location.href)
});

if (window.location.pathname !== '/'){
	console.log('not index');
	stopTimer();
}

//  Bootstrap javascript for tasks
$(".icon-time").tooltip({
  'selector': '',
  'placement': 'top'
});

