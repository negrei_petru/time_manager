App.Task = DS.Model.extend({
  title: DS.attr('string'),
  desc: DS.attr('string'),
  status : DS.attr('string',{defaultValue:"backlog"}),
  type: DS.attr('string', {defaultValue: "bug"}),
  from: DS.attr('date', {defaultValue: new Date().toString()}),
  until : DS.attr('number',{defaultValue: 5}),
  user: DS.belongsTo( 'App.User'),
  is_now: function(){
    var now = new Date()
    var startTime = new Date(this.get('from'));
    var endTime = startTime.getTime() + ( this.get('until') * 60 * 1000);
    if(now.getTime() < endTime){
      return true
    } else {
      return false
    }
  }.property('from','until'),

  today: function(){
    var now = new Date();
    var this_date = now.getDate();
    var startTime = new Date(this.get('from'));
    var task_date = startTime.getDate();

    if (this_date == task_date){
      return true;
    } else {
      return false;
    }
  }.property('@each.from'),

  this_month: function(){
    var now = new Date();
    var this_month = now.getMonth();
    var startTime = new Date(this.get('from'));
    var task_month = startTime.getMonth();

    if (this_month == task_month){
      return true;
    } else {
      return false;
    }
  }.property('@each.from'),

  this_week: function(){
    var now = new Date();
    var this_week = now.getWeek();
    var startTime = new Date(this.get('from'));
    var task_week = startTime.getWeek();

    if (this_week == task_week){
      return true;
    } else {
      return false;
    }
  }.property('@each.from'),

  end_time: function(){
    var startTime = new Date(this.get('from'));
    return startTime.getTime() + ( this.get('until') * 60 * 1000);
  }.property('from')
});

App.Task.FIXTURES = [
  {   
    id:1, title: '1', 
    desc: "Small description",  
    status:'current', type:'bug',
    from: new Date('Tue May 07 2013 13:00:00 GMT+0300 (EEST)').toString(),   
    until : 2, user:1 	
  },
  {   
    id:2, title: '2', 
    desc: "Small description",  
    status:'completed', type:'add',
    from: new Date('Tue May 07 2013 13:03:00 GMT+0300 (EEST)').toString(),   
    until : 2, user:1  
  },
  {   
    id:3, title: '3', 
    desc: "Small description",  
    status:'current', type:'improve',
    from: new Date('Tue May 07 2013 10:55:29 GMT+0300 (EEST)').toString(),   
    until : 30, user:2
  },
  {   
    id:4, title: '4', 
    desc: "Small description",  
    status:'current', type:'improve',
    from: new Date('Tue May 07 2013 10:45:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:2  
  },
  {   
    id:5, title: '5', 
    desc: "Small description",  
    status:'current', type:'bug',
    from: new Date('Tue May 07 2013 10:53:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:2  
  },
  {   
    id:6, title: '6', 
    desc: "Small description",  
    status:'current', type:'add',
    from: new Date('Tue May 07 2013 10:59:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:2  
  },
  {   
    id:7, title: '7', 
    desc: "Small description",  
    status:'current', type:'bug',
    from: new Date('Tue May 07 2013 10:45:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:3  
  },
  {   
    id:8, title: '8', 
    desc: "Small description",  
    status:'current', type:'add',
    from: new Date('Tue May 07 2013 10:45:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:3  
  },
  {   
    id:9, title: '9', 
    desc: "Small description",  
    status:'current', type:'improve',
    from: new Date('Tue May 07 2013 10:45:29 GMT+0300 (EEST)').toString(),   
    until : 20, user:3  
  },
];
