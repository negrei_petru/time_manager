App.User = DS.Model.extend({ // need to define attrs except id.
  username: DS.attr('string',{ defaultValue:"Default user"}),
  avatar: DS.attr('string',{ defaultValue:"http://www.mixcloud.com/media/images/graphics/33_Profile/default_user_300x300-v4.png?v=107"}),
  tasks: DS.hasMany( 'App.Task'),
  total: function(){
    return this.get('tasks').getEach('title').length;
  }.property('@each.tasks'),
  current: function(){
    return this.get('tasks').filterProperty("status","current").length
  }.property('tasks.@each.status'),
  completed: function(){
    return this.get('tasks').filterProperty("status","completed").length
  }.property('tasks.@each.status'),
  datas: function(){
    tasks = this.get('tasks');
    total   = tasks.getEach('title').length;
    bugs    = tasks.filterProperty("type","bug").length
    improve = tasks.filterProperty("type","improve").length
    add     = tasks.filterProperty("type","add").length

    bugpe = (bugs*100.0)/ total
    improvepe = (improve*100.0)/ total
    addpe = (add*100.0)/ total
    var data = [
        { label: "Improve", data: improvepe, color: "#4572A7"},
        { label: "Bug",     data: bugpe, color: "#AA4643"},
        { label: "Add",    data: addpe, color: "#89A54E"},
    ];
    return data
  }.property('tasks.@each.type'), 
  sorted_tasks: function(){
    var tasks = this.get('tasks').toArray();
    
    // not good, need to fix in future
    tasks.sort(function(a,b) {
        return new Date(a.get('from')).getTime() - new Date(b.get('from')).getTime()  ;
    });
    return tasks
  }.property('tasks.@each.from')

});

App.User.FIXTURES = [
  {
  	id: 1,
  	username:'Ivan Zarea',
  	avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c74.269.466.466/s160x160/67509_10151325991912083_561841746_n.jpg',
    tasks: [1,2]
  },
  {
  	id:2, 
  	username: 'Roman Roibu',
  	avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c63.63.791.791/s160x160/33976_10200778519408953_1818767410_n.jpg',
    tasks: [3,4,5,6]
  },
  {
  	id:3,
  	username: 'Victor Vasilica',
  	avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c170.50.621.621/s160x160/545972_348308938595740_841149658_n.jpg',
    tasks: [7,8,9]
  },
  {
  	id:4,
  	username: "Vlad Ledniov",
  	avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c49.49.609.609/s160x160/533919_10200245376115695_1942231326_n.jpg'
  },
  {
  	id:5,
  	username: 'Andrei Lisnic',
  	avatar: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c170.50.621.621/s160x160/537646_10200135485842358_1769064285_n.jpg'
  },
  {
  	id: 6, 
  	username: 'Ivan Danci', 
  	avatar:'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash4/c27.27.332.332/s160x160/196077_4573052697548_205045938_n.jpg',
  },
  {
    id: 7, 
    username: 'Dima', 
    avatar:'http://www.mixcloud.com/media/images/graphics/33_Profile/default_user_300x300-v4.png?v=107',
  },
  {
    id: 8, 
    username: 'Nicu', 
    avatar:'http://www.mixcloud.com/media/images/graphics/33_Profile/default_user_300x300-v4.png?v=107',
  }
];

