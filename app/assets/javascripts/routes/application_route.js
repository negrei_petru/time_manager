App.ApplicationRoute = Ember.Route.extend({
  setupController: function() {
    this.controllerFor('newtaskuser').set('model', App.User.find());
  }
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return App.User.find(); 
  },
  setupController: function(controller, model) {
  	controller.set('content', App.User.find());
  	controller.set('model', App.User.find());
  }
});

// App.Router.reopen({
//   location: 'history'
// });