App.UsersRoute = Ember.Route.extend({
  model: function() {
    return App.User.find(); 
  }
});

App.UsersIndexRoute = Ember.Route.extend({
  model: function() {
    return App.User.find(); 
  }
});

App.UsersNewRoute = Ember.Route.extend({
	model: function() {
		return App.User.createRecord();
	}
})