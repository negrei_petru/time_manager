App.TasksRoute = Ember.Route.extend({
  model: function() {
  	// need time to load
	var tasks = App.Task.find()
	// var content = tasks.filterProperty("today",true);
	// return content;
	return tasks;
  }
});

App.TasksNewRoute = Ember.Route.extend({
	model: function() {
		return App.Task.createRecord();
	},
  	setupController: function(controller, model) {
    	this.controllerFor('users').set('content', App.User.find());
  }
})