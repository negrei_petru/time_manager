App.Router.map( function () {
	
  this.resource('tasks', function() {
  	this.resource('task',{ path:':task_id'});
  	this.route('edit',{ path:'/edit/:task_id'});
  	this.route('new');
  });

  this.resource('users', function() {
  	this.resource('user',{ path:':user_id'});
  	this.route('new');
  });
});