App.TasksController = Ember.ArrayController.extend({
	init : function(){
		// set the default sorting for tasks
		this.set('sortProperties',['user']);
		// var content = this.filterProperty("today",true);
		// this.set('content',content)	
	},
	
	backlog : function() {
		return this.filterProperty("status","backlog")
	}.property("@each.status"),

	current : function() {
		return this.filterProperty("status","current")
	}.property("@each.status"),

	completed : function() {
		return this.filterProperty("status","completed")
	}.property("@each.status"),

	setupStatus: function(taskid,status){
		task = App.Task.find(taskid)
		task.set('status',status);
		task.store.commit()
	},

	delete: function( item ) {
	    var title = item.get('title');
	    if( confirm("Really delete " + title + "?") ){
	      item.store.deleteRecord( item );
	      item.store.commit();
	    }
	},

	pushSort: function(attribute){
		if (this.get('sortProperties.firstObject') == attribute ){
			this.toggleProperty('sortAscending')
		} else {
			this.set('sortProperties',[attribute]);
			this.set('sortAscending', true)
		}
	},
	pushTime: function(attribute){
		var tasks = App.Task.find()
		// console.log(tasks)
		if(attribute == 'today'){
			var content = tasks.filterProperty("today",true);
		} else if (attribute == 'month'){
			var content = tasks.filterProperty("this_month",true);
		} else if (attribute == 'week'){
			var content = tasks.filterProperty("this_week", true)
		}
		// need to add this week
		this.set('content',content)	
	}
});