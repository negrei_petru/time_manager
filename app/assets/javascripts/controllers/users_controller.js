App.UsersController = Ember.ArrayController.extend({
});

App.UsersIndexController = Ember.ArrayController.extend({
	totalTaskss : function() {
		// return an array of total tasks for each user
		var tasks = this.getEach('total')
		return tasks
	}
});

App.UsertasksController = App.TasksController.extend({});

App.UserController = Ember.ObjectController.extend({

	delete: function( item ) {
	    var username = item.get('username');
	    if( confirm("Really delete " + username + "?") ){
	      item.store.deleteRecord( item );
	      this.get('target').transitionTo('users');
	      this.store.commit();
	    }
	}
});

