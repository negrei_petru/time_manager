App.UsersNewController = Ember.ObjectController.extend({
	save: function() {
		this.get("model.transaction").commit();
		this.get('target').transitionTo('users');
	}
});

App.selectedPersonController = Ember.Object.create({
    person: null
});