App.TasksNewController = Ember.ObjectController.extend({
	save: function() {
		this.get("model.transaction").commit();
		// console.log(this.get("model").get('from'))
		this.store.commit();
		this.get('target').transitionTo('tasks');
	},
	setupType : function(task, status){
		task.set('type', status)
		task.store.commit()
	},
	setupTime : function(task, time){
		var now = new Date();
		if ( time == 'now' ){
			time = 0;
		}
		var helper = now.getTime() + ( time * 60 * 1000 )
		var finaltime = new Date(helper)
		console.log(finaltime)
		task.set('from', finaltime)
		task.store.commit()
	}
});

// inherits all the methods from task new controller
App.TasksEditController = App.TasksNewController.extend({});

// simple array used in showing type of tasks
App.Types = Ember.Object.create({
    "types": Ember.A(["add", "bug","improve"])
});


// simple array used in showing type of tasks
App.TimePicker = Ember.Object.create({
    "time": Ember.A(['now',"after", "+10","+20","+30"])
});

// this is a special created controller for choosing a user
App.NewtaskuserController =  Ember.ArrayController.extend({
	setupUser: function(taskid,user){
		task = App.Task.find(taskid);
		task.set('user',user);
		task.store.commit()
	}
});

