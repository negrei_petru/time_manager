class TaskSerializer < ActiveModel::Serializer
  attributes :id, :title, :desc, :status, :type, :from, :until, :user_id
end
