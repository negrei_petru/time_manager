class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.string :desc
      t.string :status
      t.string :type
      t.datetime :from
      t.integer :until
    end
  end
end
