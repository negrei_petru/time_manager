# -*- encoding: utf-8 -*-
# stub: handlebars-source 1.0.9 ruby lib

Gem::Specification.new do |s|
  s.name = "handlebars-source"
  s.version = "1.0.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Yehuda Katz"]
  s.date = "2013-02-20"
  s.description = "Handlebars.js source code wrapper for (pre)compilation gems."
  s.email = ["wycats@gmail.com"]
  s.homepage = "https://github.com/wycats/handlebars.js/"
  s.rubygems_version = "2.2.1"
  s.summary = "Handlebars.js source code wrapper"

  s.installed_by_version = "2.2.1" if s.respond_to? :installed_by_version
end
