# -*- encoding: utf-8 -*-
# stub: ember-template-compiler-source 1.0.0.pre4.4 ruby lib

Gem::Specification.new do |s|
  s.name = "ember-template-compiler-source"
  s.version = "1.0.0.pre4.4"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Yehuda Katz"]
  s.date = "2013-02-21"
  s.description = "Ember-Handlebars precompiler source code wrapper for use with Ruby libs."
  s.email = ["wycats@gmail.com"]
  s.homepage = "https://github.com/emberjs/ember.js"
  s.rubygems_version = "2.2.1"
  s.summary = "Ember-Handlebars precompiler source code wrapper."

  s.installed_by_version = "2.2.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<handlebars-source>, [">= 1.0.0.rc2"])
    else
      s.add_dependency(%q<handlebars-source>, [">= 1.0.0.rc2"])
    end
  else
    s.add_dependency(%q<handlebars-source>, [">= 1.0.0.rc2"])
  end
end
