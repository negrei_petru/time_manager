In this project we will build a task manager in order to improve the productivity of the team.

## Introduction

This application is development only with Ember.js as the front-end framework, as the backend
we use a Ruby on Rails.

## Description

### Users

As team leader, I can:

* Browse all tasks
* Browse all users
* Add an new user to the team
* Add new tasks and assign them to the users
* Remove an tasks and users
* Change the types of tasks and status of them

Below is represented a user page (right next to each user are represented their tasks completed or uncompleted ):

![User_page](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/full_screenshots/all2.png)

By choosing a user we can see more detailed information about them.

![User_status](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/full_screenshots/all3.png)

Here is more detailed view of users page:

![List](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/users/list.png)

![New](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/users/new.png)

![Status](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/users/status.png)

### Tasks

Below is represented the task page, witch purpose is to give the team leader possibility to add, delete, edit 
and show tasks.  

![User_page](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/full_screenshots/all.png)

![New](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/tasks/tasks_list.png)

When a new task is created by default it is assigned 'backlog status', then by draging and droping team 
leader can change the status of task.

![New](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/tasks/creating_tasks.png)

![New](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/tasks/backlog.png)

### Main page

The main page represent a time which is the only page visible to the team members, they can see
the tasks assigned to them, and the time remained also the type of tasks.

![Timerpage](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/full_screenshots/all4.png)

![TimeScale](https://bitbucket.org/negrei_petru/time_manager/raw/master/screenshots/tasks/timescale.png)
